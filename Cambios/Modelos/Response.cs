﻿namespace Cambios.Modelos
{
    public class Response
    {
        //classe para detetar se esta tudo bem, que erros teve, etc...
        public bool IsSuccess { get; set; }//diz se correu bem ou  nao

        public string Message { get; set; }//messagem no caso de correr mal

        public object Result { get; set; }//caso corra bem gurada o objecto


    }
}
