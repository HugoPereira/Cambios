﻿namespace Cambios
{
    using Modelos;
    using Servicos;
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;
    using System.Threading.Tasks;

    public partial class Form1 : Form
    {
        #region atributos
        private List<Rate> Rates;

        private NetworkServices networkServices; //chamo objecto do tipo networkservices(que tem la dentro o try do response)

        private ApiServices apiServices;

        private DialogServices dialogServices;//crio objecto mensagem

        private DataServices dataServices;//para gravar localmente
        #endregion

        //public List<Rate> Rates { get; set; } = new List<Rate>();//crio uma propriedade do tipo rates que vai receber os objectos

        public Form1()
        {
            InitializeComponent();

            networkServices = new NetworkServices();

            apiServices = new ApiServices();

            dialogServices = new DialogServices();//instancio objecto mensagem

            dataServices = new DataServices();

            LoadRates();
        }

        private async void LoadRates() //este metodo tem de ser async, por causa do await
        {
            bool load;
            //ProgressBar1.Value = 0;//progressbar inicia a 0

            LabelResultado.Text = "A actualizar taxas...";

            var connection = networkServices.CheckConnection();//variavel ganhas as propriedades do network(torna-se uma variavel do tipo response)

            if (!connection.IsSuccess)//caso nao tenha conecção
            {
                LoadLocalRates();
                load = false;
                //MessageBox.Show(connection.Message);
                //return;
            }
            else
            {
                await LoadApiRates();
                load = true;
            }

            if (Rates.Count == 0 )//na primeira ligacao, caso nao tenha internet, por isso nao tenho ainda carregada a lista local
            {
                //environment.new line mete o texto numa linha nova na label
                LabelResultado.Text = "Não há ligação à internet" + Environment.NewLine +
                    "e não foram previamente carregadas as taxas." + Environment.NewLine +
                    "Tente mais tarde!";

                LabelStatus.Text = "Primeira inicialização deverá ter ligação á internet";

                return;
            }

           // var client = new HttpClient();//crio uma ligacao http, para ir a uma ligacao externa

            //client.BaseAddress = new Uri("http://apiexchangerates.azurewebsites.net");//dou a url de base, so o site, sem a pasta expecifica

            //awaite, espera enquanto carrega, a aplicacao nao para enquanto carrega
            //var response = await client.GetAsync("/api/Rates");// getasync dou o resto do caminho, portanto a pasta onde ele esta (controlador da api)

            //var result = await response.Content.ReadAsStringAsync();//recebo o resultado da api em formato string, para dentro da 

            //if (!response.IsSuccessStatusCode)//se alguma coisa correr mal
            //{
            //    MessageBox.Show(response.ReasonPhrase);
            //    return;
            //}

            //var rates = JsonConvert.DeserializeObject<List<Rate>>(result);//pega nos resultados, convert de json e cria uma lista de objectos

            ComboBoxOrigem.DataSource = Rates;
            ComboBoxOrigem.DisplayMember = "Name";

            ComboBoxDestino.BindingContext = new BindingContext();//classe que liga objectos do interface ao codigo | serve para separa as combobox

            ComboBoxDestino.DataSource = Rates;
            ComboBoxDestino.DisplayMember = "Name";

           

           

            LabelResultado.Text = "Taxas actualizadas...";

            if (load)
            {
                LabelStatus.Text = string.Format("Taxas carregadas da internet em {0:F}", DateTime.Now);
            }
            else
            {
                LabelStatus.Text = string.Format("Taxas carregdas da base de dados.");
            }
            ProgressBar1.Value = 100; //progressbar termina a 100

            ButtonConverter.Enabled = true;
            ButtonTroca.Enabled = true;
        }

        private void LoadLocalRates()
        {
            Rates = dataServices.GetData();
        }

        private  async Task LoadApiRates()//async tem de ter um await e vice-versa
        {
            ProgressBar1.Value = 0;
            var response = await apiServices.GetRates("http://apiexchangerates.azurewebsites.net", "/api/Rates");

            Rates = (List<Rate>)response.Result;//vem como objecto e converto em lista de rates

            dataServices.DeleteData(); //apaga o que esta na bd local

            dataServices.SaveData(Rates); //grava na base dados local
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void ButtonConverter_Click(object sender, EventArgs e)
        {
            Converter();
        }

        private void Converter()
        {
            if (string.IsNullOrEmpty(TextBoxValor.Text))
            {
                dialogServices.ShowMessage("Erro", "Insira um valor a converter");
                return;
            }

            decimal valor;
            if (!decimal.TryParse(TextBoxValor.Text, out valor))
            {
                dialogServices.ShowMessage("Erro de conversão", "Valor tem de ser numérico");
                return;
            }

            if (ComboBoxOrigem.SelectedItem == null)
            {
                dialogServices.ShowMessage("Erro", "Tem de escolher uma moeda a converter");
                return;
            }

            if (ComboBoxDestino.SelectedItem == null)
            {
                dialogServices.ShowMessage("Erro", "Tem de escolher uma moeda de destino para converter");
                return;
            }

            var TaxaOrigem = (Rate) ComboBoxOrigem.SelectedItem; //faz o cast para forcar a ser um rate

            var TaxaDestino = (Rate) ComboBoxDestino.SelectedItem;

            var ValorConvertido = valor / (decimal) TaxaOrigem.TaxRate * (decimal) TaxaDestino.TaxRate;// faco o cast para forcar a ser decimal e vou buscar o taxrate

            LabelResultado.Text = string.Format("{0} {1:C2} = {2} {3:C2}", TaxaOrigem.Code, valor, TaxaDestino.Code, ValorConvertido);
        }

        private void ButtonTroca_Click(object sender, EventArgs e)
        {
            Troca();
        }

        private void Troca()
        {
            var aux = ComboBoxOrigem.SelectedItem;
            ComboBoxOrigem.SelectedItem = ComboBoxDestino.SelectedItem;
            ComboBoxDestino.SelectedItem = aux;
            Converter();
        }
    }
}
