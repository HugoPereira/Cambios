﻿namespace Cambios.Servicos
{
    using System.Windows.Forms;

    public class DialogServices
    {
        public void ShowMessage(string title, string message)
        {
            MessageBox.Show(message, title);
        }
    }
}
