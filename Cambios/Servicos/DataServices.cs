﻿namespace Cambios.Servicos
{
    using Modelos;
    using System;
    using System.Collections.Generic;
    using System.Data.SQLite;
    using System.Globalization;
    using System.IO;


    public class DataServices
    {
        //atributos
        private SQLiteConnection connection; //conexao sql

        private SQLiteCommand command; //comando sql

        private DialogServices dialogServices;

        //construtores
        public DataServices()
        {
            dialogServices = new DialogServices();

            if (!Directory.Exists("Data")) //se a pasta nao estiver criada
            {
                Directory.CreateDirectory("Data"); //cria a pasta
            }

            var Path = @"Data\Rates.sqlite";//caminho para gravar localmente

            try
            {
                connection = new SQLiteConnection("Data Source=" + Path); //conexao a base de dados
                connection.Open(); //abre a base de dados ou cria se nao existir

                string sqlcommand = "create table if not exists rates(RateId int, Code varchar(5), TaxRate real, Name varchar(250))"; //comando sql para criar a tabela com os campos

                command = new SQLiteCommand(sqlcommand, connection);

                command.ExecuteNonQuery(); //executa o comando
            }
            catch (Exception e)
            {
                dialogServices.ShowMessage("Erro", e.Message);
            }
        }

        public void SaveData(List<Rate> Rates)
        {
            try
            {
                CultureInfo ci = new CultureInfo("en-us");//resolve problema do teclado ao gravar taxrate
                System.Threading.Thread.CurrentThread.CurrentCulture = ci;

                foreach (var rate in Rates)
                {
                    //insere na tabela da base de dados os valores de cada campo
                    string sql = string.Format("insert into Rates (RateId, Code, TaxRate, Name) values ({0}, '{1}', {2}, '{3}')", rate.RateId, rate.Code, rate.TaxRate, rate.Name);

                    command = new SQLiteCommand(sql,connection);

                    command.ExecuteNonQuery();  //executa o comando
                }
                connection.Close(); //fecha-se a conexao

                ci = new CultureInfo("pt-pt");
                System.Threading.Thread.CurrentThread.CurrentCulture = ci;
            }
            catch (Exception e)
            {
                dialogServices.ShowMessage("Erro", e.Message); 
            }
        }

        public List<Rate> GetData()
        {
            List<Rate> rates = new List<Rate>();

            try
            {
                string sql = "select RateId, Code, TaxRate, Name from Rates"; //selecionar estes campos da tabela rates

                command = new SQLiteCommand(sql, connection);

                //le cada registo (linha)
                SQLiteDataReader reader = command.ExecuteReader(); //faz a leitura do fix

                while (reader.Read())//enquanto existir linhas
                {
                    rates.Add(new Rate
                    {
                        RateId = (int) reader["RateId"],
                        Code = (string) reader["Code"],
                        TaxRate = (double) reader["TaxRate"], 
                        Name = (string) reader["Name"]
                    });
                }

                connection.Close();

                return rates;

            }
            catch (Exception e)
            {
                dialogServices.ShowMessage("Erro", e.Message);
                return null;
            }
        }

        public void DeleteData()
        {
            try
            {
                string sql = "delete from Rates";

                command = new SQLiteCommand(sql, connection);

                command.ExecuteNonQuery();


            }
            catch (Exception e)
            {
                dialogServices.ShowMessage("Erro", e.Message);
            }
        }
    }
}
