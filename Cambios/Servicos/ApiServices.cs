﻿namespace Cambios.Servicos
{
    using Modelos;
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Threading.Tasks;

    public class ApiServices
    {
        public async Task<Response> GetRates(string urlBase, string controller)//faz uma tarefa do tipo response //async faz isto sem ir abaixo a aplicacao
        {
            try
            {
                var client = new HttpClient();//crio uma ligacao http, para ir a uma ligacao externa

                client.BaseAddress = new Uri(urlBase);//dou a url de base, so o site, sem a pasta expecifica

                var response = await client.GetAsync(controller);// getasync dou o resto do caminho, portanto a pasta onde ele esta (controlador da api)

                var result = await response.Content.ReadAsStringAsync();//recebo o resultado da api em formato string, para dentro da 

                if (!response.IsSuccessStatusCode)//se alguma coisa correr mal
                {
                    return new Response
                    {
                        IsSuccess = false,
                        Message = result
                    };
                }

                var rates = JsonConvert.DeserializeObject<List<Rate>>(result);//pega nos resultados, convert de json e cria uma lista de objectos

                return new Response
                {
                    IsSuccess = true,
                    Result = rates
                };

            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message
                };
            }
        }
    }
}
