﻿namespace Cambios.Servicos
{
    using Modelos;
    using System.Net;

    public class NetworkServices
    {
        public Response CheckConnection()
        {
            //objecto do tipo response(classe response)

            var client = new WebClient();//testa se tem ligacao a net

            try
            {
                using (client.OpenRead("http://clients3.google.com/generate_204"))//testa a conecçao ping ao servidor da google
                {
                    //retorna um novo objecto do tipo response, em que a propriedade issuccess e igual a true(ligacao conseguida)
                    return new Response
                    {
                        IsSuccess = true
                    };
                }
            }
            catch 
            {
                //caso nao tenha ligacao cria objecto do tipo response a dizer que nao resultou
                return new Response
                {
                    IsSuccess = false,
                    Message = "Configure a sua ligação a internet"
                };
            }
        }
    }
}
